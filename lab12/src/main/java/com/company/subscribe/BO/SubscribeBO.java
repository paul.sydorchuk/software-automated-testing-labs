package com.company.subscribe.BO;

import com.company.subscribe.PO.SubscribePO;
import io.qameta.allure.Step;

public class SubscribeBO {

    @Step("isOpen")
    public boolean subscribe() {

        SubscribePO subscribePO = new SubscribePO()
                .goToUserPage();

        return subscribePO.isOpen()
                &&
                subscribePO
                        .subscribe()
                        .isSubscribed();
    }
    @Step("isOpen")
    public boolean unsubscribe() {

        SubscribePO subscribePO = new SubscribePO()
                .goToUserPage();

        return subscribePO.isOpen()
                &&
                subscribePO
                        .unsubscribe()
                        .isUnsubscribed();
    }
}