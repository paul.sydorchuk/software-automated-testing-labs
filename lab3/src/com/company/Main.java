package com.company;

import java.util.ArrayList;
import java.util.Arrays;

public class Main {

    public static void main(String[] args) {
        int[][] sortingData = {
            {9, 8, 7, 6, 5, 4, 3, 2, 1},
            {5, 3, 2, 6, 1, 4, 7, 9, 8},
            {20, 19, 18, 17, 16, 15, 14, 13, 12, 11, 10}
        };

        ArrayList<SortingAlgorithm> sortingAlgorithms = new ArrayList<SortingAlgorithm>() {
            {
                add(new QuickSort());
                add(new BubbleSort());
                add(new InsertionSort());
            }
        };

        for (SortingAlgorithm algorithm :
                sortingAlgorithms) {

            for (int i = 0; i < sortingData.length; ++i) {
                System.out.print("Array " + (i + 1) + ": " +  Arrays.toString(sortingData[i]) + " -> ");
                algorithm.sort(sortingData[i]);
            }
        }
    }
}
