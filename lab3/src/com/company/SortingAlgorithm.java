package com.company;

public interface SortingAlgorithm {
    public void sort(int[] array);
}
