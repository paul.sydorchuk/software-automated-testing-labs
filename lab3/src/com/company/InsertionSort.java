package com.company;

import java.util.Arrays;

public class InsertionSort implements SortingAlgorithm {

    public void sort(int[] array) {
        int[] arrayCopy = new int[array.length];
        System.arraycopy(array, 0, arrayCopy, 0, arrayCopy.length);
        this.insertionSort(arrayCopy);
        System.out.println(Arrays.toString(arrayCopy));
    }

    public void insertionSort(int[] array) {
        int n = array.length;
        for (int j = 1; j < n; j++) {
            int key = array[j];
            int i = j - 1;
            while ((i > -1) && (array[i] > key)) {
                array[i + 1] = array[i];
                i--;
            }
            array[i + 1] = key;
        }
    }
}
