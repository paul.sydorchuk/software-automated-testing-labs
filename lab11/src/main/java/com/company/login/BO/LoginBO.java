package com.company.login.BO;


import com.company.PropertyUtil;
import com.company.login.PO.LoginPO;
import io.qameta.allure.Step;

public class LoginBO {



    @Step("isOpen")
    public boolean login() {

        LoginPO loginPO = new LoginPO()
                .goToLoginPage();

        return loginPO.isOpen()
                &&
                loginPO
                        .inputLogin((String) new PropertyUtil().getProperty("login"))
                        .inputPassword((String) new PropertyUtil().getProperty("pass"))
                        .submit()
                        .isOpen();
    }
    public boolean loginFailure() {

        LoginPO loginPO = new LoginPO()
                .goToLoginPage();

        return loginPO.isOpen()
                &&
                loginPO
                        .inputLogin((String) new PropertyUtil().getProperty("login"))
                        .inputPassword((String) new PropertyUtil().getProperty("pass1"))
                        .submit()
                        .isOpen();
    }
}